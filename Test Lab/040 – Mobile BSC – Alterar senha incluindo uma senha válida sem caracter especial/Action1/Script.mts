﻿'-------------------------------------------------------------------------------------------------
'-		TI DIGITAL QA
'-		Última alteração: Ricardo Cremonez/ Natália Nuñez
'-		Data: 02/01/2019
'-------------------------------------------------------------------------------------------------
'Captura caminho do cenário no Test Plan
'caminho = QCUtil.currentTest.Field("TS_SUBJECT").Path

'Substitui a raiz "Subject" por "[ALM\Resources] Resources\"
'caminho = Replace(caminho,"Subject\","[ALM\Resources] Resources\")

'Captura nome do step no Test Plan
'caso_teste = QCUtil.currentTest.Name

'Separa somente o número do caso de teste. Ex: "CT001"

'caso_teste = Mid(caso_teste,6,5)
'-------------------------------------------------------------------------------------------------
'Carrega biblioteca do cenário que contém os steps em funções
LoadFunctionLibrary "C:\Scripts\Test Plan\BSC\040 - Mobile BSC - Alterar senha incluindo uma senha valida sem caracter especial.qfl"

caso_teste = "CT001"
Call executa_step(caso_teste)

ExitTest
'-------------------------------------------------------------------------------------------------
