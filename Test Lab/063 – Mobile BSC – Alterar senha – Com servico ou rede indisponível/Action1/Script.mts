﻿'-------------------------------------------------------------------------------------------------
'-		TI DIGITAL QA
'-		Última alteração: Natália Nuñez
'-		Data: 07/01/2019
'-------------------------------------------------------------------------------------------------
'Captura caminho do cenário no Test Plan
'caminho = QCUtil.currentTest.Field("TS_SUBJECT").Path

'Substitui a raiz "Subject" por "[ALM\Resources] Resources\"
'caminho = Replace(caminho,"Subject\","[ALM\Resources] Resources\")

'Captura nome do step no Test Plan
'caso_teste = QCUtil.currentTest.Name

'Separa somente o número do caso de teste. Ex: "CT001"

'caso_teste = Mid(caso_teste,6,5)
'-------------------------------------------------------------------------------------------------
'Carrega biblioteca do cenário que contém os steps em funções
LoadFunctionLibrary "C:\Scripts\Test Plan\BSC\063 – Mobile BSC – Alterar senha – Com servico ou rede indisponível.qfl"

caso_teste = "CT001"
Call executa_step(caso_teste)


ExitTest
'-------------------------------------------------------------------------------------------------

